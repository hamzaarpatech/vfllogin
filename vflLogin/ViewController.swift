//
//  ViewController.swift
//  vflLogin
//
//  Created by Hamza on 2/18/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var orientation : Bool? = true
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        loadVfl()
    }
    
    func loadVfl(){
    let myDictionary = ["textfield1": userTextLoad() , "textfield2":userPassLoad() , "signinBt": signinBtLoad() , "signupBt":signupBtLoad() , "logoImage":logoLoad()] as [String : Any]

    // for portrait mode
    if orientation == true{
        for myLayout in myDictionary.keys{
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-40-[\(myLayout)]-40-|", options: [], metrics: nil, views: myDictionary))
        }
        let metrics = ["myLogoHeight":100 , "myButtonHeight":70 , "textFieldHeight":60]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(200@699)-[logoImage(myLogoHeight@999)]-50-[textfield1(textFieldHeight@999)]-[textfield2(textfield1)]-50-[signinBt(myButtonHeight@999)]-[signupBt(signinBt)]-(>=50)-|", options: [], metrics: metrics, views: myDictionary))
        }
     //for landscape mode
    else{
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(110@699)-[logoImage]-|", options: [], metrics: nil, views: myDictionary))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(110@699)-[textfield1]-[textfield2]-[signinBt]-[signupBt]-(>=50)-|", options: [], metrics: nil, views: myDictionary))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-60-[logoImage(100@699)]-40-[textfield1(==logoImage)]", options: [], metrics: nil, views: myDictionary))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-60-[logoImage(100@699)]-40-[textfield2(==logoImage)]", options: [], metrics: nil, views: myDictionary))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-60-[logoImage(100@699)]-40-[signinBt(==logoImage)]", options: [], metrics: nil, views: myDictionary))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-60-[logoImage(100@699)]-40-[signupBt(==logoImage)]", options: [], metrics: nil, views: myDictionary))
        }
        
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            removeAllConstraints()
            orientation = false
            loadVfl()
        } else {
            print("Portrait")
            removeAllConstraints()
            orientation = true
            loadVfl()
        }
    }
}
