//
//  ExtensionData.swift
//  vflLogin
//
//  Created by Hamza on 2/19/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit

extension ViewController {
    func userTextLoad()->UITextField{
                let username = UITextField()
                username.translatesAutoresizingMaskIntoConstraints = false
                username.textColor = .black
                username.backgroundColor = .cyan
                username.textAlignment = .center
                username.text = "Write Username Here"
                username.textColor = UIColor.lightGray
                username.layer.cornerRadius = 9.0
                username.returnKeyType = .next
                view.addSubview(username)
                return username
      }
      
      func userPassLoad()->UITextField{
                 let password = UITextField()
                 password.translatesAutoresizingMaskIntoConstraints = false
                 password.textColor = .black
                 password.backgroundColor = .red
                 password.textAlignment = .center
                 password.text = "*******"
                 password.textColor = UIColor.lightGray
                 password.layer.cornerRadius = 9.0
                 password.returnKeyType = .done
                 view.addSubview(password)
                 return password
      }
      
      func logoLoad()->UIImageView{
                 let logoImage = UIImageView()
                 logoImage.translatesAutoresizingMaskIntoConstraints = false
                 logoImage.image = #imageLiteral(resourceName: "Arpatech-footer")
                 view.addSubview(logoImage)
                 return logoImage
      }
      
      func signinBtLoad()->UIButton{
                 let signinBt = UIButton()
                 signinBt.translatesAutoresizingMaskIntoConstraints = false
                 signinBt.setTitle("Sign In", for: .normal)
                 signinBt.setTitleColor(.black, for: .normal)
                 signinBt.layer.cornerRadius = 9.0
                 signinBt.backgroundColor = .yellow
                 view.addSubview(signinBt)
                 return signinBt
      }
      
      func signupBtLoad()->UIButton{
                 let signupBt = UIButton()
                 signupBt.translatesAutoresizingMaskIntoConstraints = false
                 signupBt.setTitle("Sign Up", for: .normal)
                 signupBt.setTitleColor(.black, for: .normal)
                 signupBt.layer.cornerRadius = 9.0
                 signupBt.backgroundColor = .orange
                 view.addSubview(signupBt)
                 return signupBt
      }
    
    public func removeAllConstraints() {
           self.view.removeConstraints(self.view.constraints)
           self.view.subviews.forEach({ $0.removeFromSuperview() }) // remove all subviews
           self.view.translatesAutoresizingMaskIntoConstraints = true
       }
}

